#pragma once
/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 1,
    READ_INVALID_BITS = 2,
    READ_INVALID_HEADER = 3
    /* коды других ошибок  */
};

enum read_from_file_status  {
    SUCCESS = 0,
    UNABLE_TO_READ = 1
};



/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR = 4
    /* коды других ошибок  */
};
