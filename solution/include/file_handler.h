
#include <stdio.h>
#include <stdlib.h>
#pragma once
FILE * open_for_read(const char *file);
FILE * open_for_write(const char *file);
int close(FILE* file);

