#include "enums.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma once  // save the original data alignment
struct __attribute__((packed))bmp_header {
    uint16_t bfType; // specifies the file type 0x4D42
    uint32_t bfileSize; // specifies the size in bytes of the bitmap file
    uint32_t bfReserved; // reserved; must be 0
    uint32_t bOffBits; // specifies the offset in bytes from the bitmapfileheader to the bitmap bits
    uint32_t biSize; // specifies the number of bytes required by the struct 40(BITMAPINFOHEADER) или 108(BITMAPV4HEADER) или 124(BITMAPV5HEADER)
    uint32_t biWidth; //specifies width in pixels
    uint32_t biHeight; //specifies height in pixels
    uint16_t biPlanes; //specifies the number of color planes, must be 1
    uint16_t biBitCount; // specifies the number of bits per pixel 0 | 1 | 4 | 8 | 16 | 24 | 32
    uint32_t biCompression; // specifies the type of compression BI_RGB | BI_RLE8 | BI_RLE4 | BI_BITFIELDS | BI_JPEG | BI_PNG
    uint32_t biSizeImage; // size of image in bytes (обычно 0)
    uint32_t biXPelsPerMeter; // number of pixels per meter in x axis
    uint32_t biYPelsPerMeter; // number of pixels per meter in y axis
    uint32_t biClrUsed; // number of colors used by the bitmap
    uint32_t biClrImportant; // number of colors that are important 0
};


struct pixel { uint8_t b, g, r; };

struct image {
    uint32_t width, height;
    struct pixel* data;
};
//pragma pack(pop)  // restore the previous pack setting
struct bmp_image {
    struct bmp_header header;
    struct image image;
};

enum read_status from_bmp(FILE* in, struct image* img );


enum write_status to_bmp(FILE* out, const struct image* img );

