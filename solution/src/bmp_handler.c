#include "../include/enums.h"
#include "../include/bmp_handler.h"

struct bmp_header create_stock_header(const struct image* image) {
    struct bmp_header bmp_header = {
            .bfType = 0x4D42,
            .bfileSize = image->height * image->width * sizeof(struct pixel) +
                         image->height * (image->width % 4) * sizeof(struct pixel),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage =  image->height * image->width * sizeof(struct pixel) + (image->width % 4) * image->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    return bmp_header;
}

enum read_status from_bmp(FILE* in, struct image* img ){
    struct bmp_header *header = malloc(sizeof (struct bmp_header));
    printf("STARTING READING PROCESS 2\n");
    if (in == NULL){ printf("null");}

    if (fread(header, sizeof(struct bmp_header), 1, in) == 1){
        printf("READING SUCCESSFULLY");
        img->width = header->biWidth;
        img->height = header->biHeight;
        img->data = malloc(img->height * img->width * sizeof(struct pixel));
        for (size_t i = 0; i < img->height; i++) {
            fread(&(img->data[i * img->width]), sizeof(struct pixel), img->width, in);
            fseek(in, (uint8_t) img->width % 4, SEEK_CUR);
        }
        free(header);
        return READ_OK;
    } else return READ_INVALID_HEADER;
}

enum write_status to_bmp(FILE* out, const struct image * img ) {
    struct bmp_header header = create_stock_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) == 1) {
        const size_t fill = 0;
        for (uint32_t i = 0; i < img->height; i++) {
            fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out);
            fwrite(&fill, 1, img->width % 4, out);
        }
        return WRITE_OK;
    }
    else return WRITE_ERROR;
}
