#include "../include/enums.h"
#include "../include/file_handler.h"

FILE * open_for_read(const char *file){
    return fopen(file, "rb");
}

FILE * open_for_write(const char *file){
    return fopen(file, "wb");
}
int close(FILE* file){
    return fclose(file);
}
