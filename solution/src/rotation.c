#include "../include/bmp_handler.h"
#include "../include/rotation.h"

struct image * rotate(struct image * flat){
    struct image* rotated = malloc(sizeof (struct image));
    rotated->data = malloc(sizeof(struct pixel) * flat->height * flat->width);
    rotated->height = flat->width;
    rotated->width = flat->height;

    for (size_t i = 0; i < flat->width; i++) {
        for (size_t j = 0; j < flat->height; j++) {
            rotated->data[i*rotated->width+j] = (flat->data)[(rotated->width-j-1)*rotated->height+i];
        }
    }
    return rotated;
}
