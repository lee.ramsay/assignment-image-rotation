#include "file_handler.h"
#include "bmp_handler.h"
#include "enums.h"
#include "rotation.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc == 3){

        FILE *file_for_read = open_for_read(argv[1]);
        FILE *file_for_write = open_for_write(argv[2]);
        if(!file_for_write||!file_for_read){return READ_INVALID_SIGNATURE;}


        struct image * image_1 = malloc(sizeof (struct image));
        if (from_bmp(file_for_read, image_1) != READ_OK){
            return 2;
        }

        struct image * rotated_image = rotate(image_1);
        close(file_for_read);
        to_bmp(file_for_write, rotated_image);
        close(file_for_write);
        free(image_1->data);
        free(image_1);
        free(rotated_image->data);
        free(rotated_image);
    }

    return 0;
}
